import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([])
  const loadingStore = useLoadingStore()
  const initialUser: User = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male'
    // roles: ['user']
  }
  const editedUser = ref<User>(JSON.parse(JSON.stringify(initialUser)))

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    console.log(editedUser.value)
    loadingStore.finish()
  }

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    console.log(res.data)
    loadingStore.finish()
  }

  async function saveUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    if (!user.id) {
      //Add new
      const res = await userService.addUser(user)
    } else {
      // update
      console.log(user)
      const res = await userService.updateUser(user)
    }
    clearForm()
    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.delUser(user)
    clearForm()
    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }
  return { users, editedUser, getUser, getUsers, saveUser, deleteUser, clearForm }
})
