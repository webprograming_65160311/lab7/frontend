import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import { useLoadingStore } from './loading'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()

  async function callConvert() {
    loadingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log('Error: ' + e)
    }
    loadingStore.finish()
  }
  return { valid, result, celsius, callConvert }
})
